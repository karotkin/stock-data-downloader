package cz.karotkin.stock_downloader.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;

@Service
public class StockService {

    private static final String API_KEY_ALPHA_VANTAGE = "JED1LRNR2H8AG2WT";

    public byte[] fetchStockData(String ticker) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Workbook workbook = new XSSFWorkbook();

        // Fetch and write daily data
        JsonNode dailyData = fetchDataFromAlphaVantage(ticker, "TIME_SERIES_DAILY", "full");
        addDataToSheet(workbook, dailyData, "Daily");

        // Fetch and write weekly data
        JsonNode weeklyData = fetchDataFromAlphaVantage(ticker, "TIME_SERIES_WEEKLY", "full");
        addDataToSheet(workbook, weeklyData, "Weekly");

        // Fetch and write monthly data
        JsonNode monthlyData = fetchDataFromAlphaVantage(ticker, "TIME_SERIES_MONTHLY", "full");
        addDataToSheet(workbook, monthlyData, "Monthly");

        workbook.write(outputStream);
        workbook.close();
        return outputStream.toByteArray();
    }

    private JsonNode fetchDataFromAlphaVantage(String ticker, String function, String outputSize) throws IOException {
        String url = String.format("https://www.alphavantage.co/query?function=%s&symbol=%s&outputsize=%s&apikey=%s", function, ticker, outputSize, API_KEY_ALPHA_VANTAGE);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(url);
        CloseableHttpResponse response = httpClient.execute(request);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(response.getEntity().getContent());
        response.close();

        return rootNode;
    }

    private void addDataToSheet(Workbook workbook, JsonNode data, String sheetName) {
        Sheet sheet = workbook.createSheet(sheetName);
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Date");
        header.createCell(1).setCellValue("Open");
        header.createCell(2).setCellValue("High");
        header.createCell(3).setCellValue("Low");
        header.createCell(4).setCellValue("Close");
        header.createCell(5).setCellValue("Volume");

        JsonNode timeSeries = data.get("Time Series (Daily)");
        if (timeSeries == null) {
            timeSeries = data.get("Weekly Time Series");
        }
        if (timeSeries == null) {
            timeSeries = data.get("Monthly Time Series");
        }

        int rowIdx = 1;
        Iterator<String> dates = timeSeries.fieldNames();
        while (dates.hasNext()) {
            String date = dates.next();
            JsonNode dailyData = timeSeries.get(date);

            Row row = sheet.createRow(rowIdx++);
            row.createCell(0).setCellValue(date);
            row.createCell(1).setCellValue(dailyData.get("1. open").asDouble());
            row.createCell(2).setCellValue(dailyData.get("2. high").asDouble());
            row.createCell(3).setCellValue(dailyData.get("3. low").asDouble());
            row.createCell(4).setCellValue(dailyData.get("4. close").asDouble());
            row.createCell(5).setCellValue(dailyData.get("5. volume").asLong());
        }
    }
}