package cz.karotkin.stock_downloader.controller;

import cz.karotkin.stock_downloader.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@Controller
public class StockController {

    @Autowired
    private StockService stockService;

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @PostMapping("/download")
    public ResponseEntity<InputStreamResource> download(@RequestParam String ticker) {
        try {
            byte[] data = stockService.fetchStockData(ticker);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
            InputStreamResource file = new InputStreamResource(byteArrayInputStream);

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + ticker + ".xlsx")
                    .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                    .body(file);
        } catch (IOException e) {
            return ResponseEntity.status(500).build();
        }
    }
}