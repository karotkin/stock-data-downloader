package cz.karotkin.stock_downloader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockDownloaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockDownloaderApplication.class, args);
	}

}
